export class AppComputerInformation {

    elComputerInformation = document.getElementById('selected-computer-information')
    computer;

    setComputer(computer) {
        this.computer = computer
        this.render()
    }

    render() {

        if (!this.computer) {
            return;
        }

        // Clear the current computer
        this.elComputerInformation.innerHTML = ''

        // Create a new container for the computer to be rendered in.
        const elComputer = document.createElement('div')

        const elComputerImageContainer = document.createElement('div')
        const image = new Image()
        image.width = 320
        image.onload = () => {
            elComputerImageContainer.appendChild( image )
        }

        image.src = this.computer.image
        elComputer.appendChild( elComputerImageContainer )

        const elComputerName = document.createElement('h1')
        elComputerName.innerText = this.computer.name
        elComputer.appendChild( elComputerName )

        const elComputerPrice = document.createElement('h4')
        elComputerPrice.innerText = `NOK ${this.computer.price}`
        elComputer.appendChild( elComputerPrice )

        const elComputerDescription = document.createElement('p')
        elComputerDescription.innerText = this.computer.description
        elComputer.appendChild( elComputerDescription )

        const elComputerRating = document.createElement('span')
        elComputerRating.innerText = `Rating: ${this.computer.rating}/10`
        elComputer.appendChild( elComputerRating )

        this.elComputerInformation.appendChild( elComputer )
    }
}