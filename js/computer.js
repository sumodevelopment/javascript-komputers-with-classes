import { fetchComputers } from "./api.js";
import { AppComputerInformation } from './computer-info.js'

export class AppComputer {

    // HTML Elements
    elComputerSelect = document.getElementById('computers')
    
    // Data properties
    computers = []
    selectedComputer = null

    // Computer components
    selectedComputerInformation = new AppComputerInformation()
    
    // Status properties
    error = ''

    async init() {
        this.createEventListeners()
        await this.createComputers()        
    }

    async createComputers() {
        try {
            this.elComputerSelect.disabled = true
            this.computers = await fetchComputers()
                .then(computers =>
                    computers.map(computer => new Computer(computer))
                )
        } catch (e) {
            this.error = e.message
            console.log(e);
        }
    }

    createEventListeners() {
        // Event Listeners
        this.elComputerSelect.addEventListener('change', this.onComputerChange.bind(this))
    }

    onComputerChange() {

        if (parseInt(this.elComputerSelect.value) === -1) {
            // Reset the current computer.
            return;
        }

        const selectedComputer = this.computers.find(computer => {
            return computer.id == this.elComputerSelect.value
        })

        this.selectedComputerInformation.setComputer( selectedComputer )
    }

    createDefaultOptionForSelect() {
        const elComputer = document.createElement('option')
        elComputer.innerText = '-- Select a computer --'
        elComputer.value = -1
        this.elComputerSelect.appendChild(elComputer)
    }

    render() {
       
        this.createDefaultOptionForSelect()

        this.computers.forEach(computer => {
            this.elComputerSelect.appendChild(computer.createComputerSelectOption())
        })

        this.elComputerSelect.disabled = false

        this.selectedComputerInformation.render()
    }
}

export class Computer {
    constructor(computer) {
        this.id = computer.id
        this.name = computer.name
        this.description = computer.description
        this.image = computer.image
        this.rating = computer.rating
        this.specs = computer.specs
        this.price = computer.price
    }

    createComputerSelectOption() {
        const elComputerOption = document.createElement('option')
        elComputerOption.value = this.id
        elComputerOption.innerText = this.name
        return elComputerOption
    }
}