import { AppComputer } from './computer.js'

class App {
    constructor() {
        this.elStatus = document.getElementById('app-status')
        this.computer = new AppComputer()
    }

    async init() {
        await this.computer.init()
        this.render()
    }

    render() {
        this.computer.render()
    }
}

new App().init()